#include <cstdio>

const int MOD = 1e9 + 7;
using ll = long long;

int get(int l, int r) {
    return (((ll)l + r) * (r - l + 1) / 2) % MOD;
}

int main(void) {
    int T;
    scanf("%d", &T);
    while (T -- > 0) {
        int n;
        scanf("%d", &n);
        int ans = 0, lst = n + 1;
        for (int i = 1; i*i <= n; ++ i) {
            int l = n / (i + 1) + 1;
            int r = n / i;
            ans = (ans + get(l, r) * (ll)i % MOD) % MOD;
            if (l < lst) lst = l;
        }
        for (int i = 1; i < lst; ++ i) {
            ans = (ans + n/i*i) % MOD;
        }
        printf("%d\n", ans);
    }

    return 0;
}
